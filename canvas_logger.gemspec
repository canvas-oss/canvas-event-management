
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'canvas_logger/version'

Gem::Specification.new do |spec|
  spec.name          = 'canvas_logger'
  spec.version       = CanvasLoggerConst::VERSION
  spec.authors       = ['Chill3d']
  spec.email         = ['chill3d@protonmail.com']
  spec.date          = '2017-11-04'

  spec.summary       = 'API for Canvas Log system'
  spec.description   = 'A little API to send logs to Syslog'
  spec.homepage      = 'https://bitbucket.org/canvas-oss/canvas-event-management'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_development_dependency 'rake', '~> 10.0'
end
