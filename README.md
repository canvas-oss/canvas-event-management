# CANVAS

CANVAS Common Application & Network Vulnerability Assessment System is a computer program whose purpose is to to identify and assess known vulnerabilities on an information system.

## CANVAS EVENT MANAGEMENT

CANVAS EVENT MANAGEMENT is a component of CANVAS. It include the CANVAS's event management system in order to handle system's logs.

## License

CANVAS including all its components is distributed under the CeCILL licence. Please see the [LICENSE.CECILL-FR](https://bitbucket.org/canvas-oss/canvas-core/src/master/LICENSE.CECILL-FR) and [LICENCE-CECILL-EN](https://bitbucket.org/canvas-oss/canvas-core/src/master/LICENSE.CECILL-EN) documents for more informations.

## Documentation

The CANVAS general documentation is available at the main [repository's wiki](https://bitbucket.org/canvas-oss/canvas/wiki/browse/).

A specific documentation is available on this [repository's wiki](https://bitbucket.org/canvas-oss/canvas-event-management/wiki/browse/).

## Installation steps

Add this line to your application's Gemfile:

```ruby
gem 'canvas_logger'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install canvas_logger

## Usage

A quick example of this API :
```ruby
example_log = CanvasLogger.new('canvas_web', 1)
example_log.log('warn', 'This is the warning log text')
example_log.close
```

## Contribution guidelines

## Contact

### Issues

You can open an issue at the [CANVAS's JIRA Dashboard](https://canvas-oss.atlassian.net/projects/CANVAS/issues). You have to be logged in to open issue.

### Development team

Dylan TROLES => chill3d@protonmail.com
